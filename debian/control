Source: mmseqs2
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Shayan Doust <hello@shayandoust.me>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Arch: cmake,
                    architecture-is-64-bit,
                    architecture-is-little-endian,
                    libgzstream-dev,
                    libzstd-dev,
                    zlib1g-dev,
                    libbz2-dev,
                    libsimde-dev,
                    libxxhash-dev
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/mmseqs2
Vcs-Git: https://salsa.debian.org/med-team/mmseqs2.git
Homepage: https://github.com/soedinglab/MMseqs2
Rules-Requires-Root: no

Package: mmseqs2
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends}
Built-Using: ${simde:Built-Using}
Description: ultra fast and sensitive protein search and clustering
 MMseqs2 (Many-against-Many sequence searching) is a software suite to
 search and cluster huge proteins/nucleotide sequence sets. MMseqs2 is
 open source GPL-licensed software implemented in C++ for Linux, MacOS,
 and (as beta version, via cygwin) Windows. The software is designed to
 run on multiple cores and servers and exhibits very good scalability.
 MMseqs2 can run 10000 times faster than BLAST. At 100 times its speed it
 achieves almost the same sensitivity. It can perform profile searches
 with the same sensitivity as PSI-BLAST at over 400 times its speed.

Package: mmseqs2-examples
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Recommends: mmseqs2
Description: optional resources for the mmseqs2 package
 MMseqs2 (Many-against-Many sequence searching) is a software suite to
 search and cluster huge proteins/nucleotide sequence sets. MMseqs2 is
 open source GPL-licensed software implemented in C++ for Linux, MacOS,
 and (as beta version, via cygwim) Windows. The software is designed to
 run on multiple cores and servers and exhibits very good scalability.
 MMseqs2 can run 10000 times faster than BLAST. At 100 times its speed it
 achieves almost the same sensitivity. It can perform profile searches
 with the same sensitivity as PSI-BLAST at over 400 times its speed.
