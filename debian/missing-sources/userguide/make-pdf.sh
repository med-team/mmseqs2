#!/bin/sh -e

cat Home.md \
	| sed '1,/<!--- TOC END -->/d' \
	| cat .pandoc/meta.yaml - \
	| pandoc \
         --from=markdown \
		 -o userguide.pdf \
		 --template=.pandoc/eisvogel.tex \
		 --toc

chmod a+r userguide.pdf

